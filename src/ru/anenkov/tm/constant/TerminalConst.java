package ru.anenkov.tm.constant;

public final class TerminalConst {

    public static final String HELP = "Help";

    public static final String VERSION = "Version";

    public static final String ABOUT = "About";

}
